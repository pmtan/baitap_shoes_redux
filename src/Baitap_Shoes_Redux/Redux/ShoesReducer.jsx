import { data_shoes } from "../Data_Shoes";
import { ADD_TO_CART, HANDLE_QUANTITY, XOA } from "./Constants";

const initialState = {
  shoesList: data_shoes,
  cart: [],
};

export const ShoesReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let newCart = [...state.cart];
      let newItemToCart = { ...payload, quantity: 1 };
      let index = newCart.findIndex((item) => item.id === payload.id);
      if (index !== -1) {
        newCart[index].quantity++;
      } else {
        newCart.push(newItemToCart);
      }
      return {
        ...state,
        cart: newCart,
      };
    }
    case XOA: {
      let index = state.cart.findIndex((item) => item.id === payload);
      let updateCart = [...state.cart];
      updateCart.splice(index, 1);
      return {
        ...state,
        cart: updateCart,
      };
    }
    case HANDLE_QUANTITY: {
      let index = state.cart.findIndex((item) => item.id === payload.id);
      let updateCart = [...state.cart];
      updateCart[index].quantity += payload.number;
      if (updateCart[index].quantity < 1) {
        updateCart.splice(index, 1);
      }
      return {
        ...state,
        cart: updateCart,
      };
    }
    default:
      return state;
  }
};
