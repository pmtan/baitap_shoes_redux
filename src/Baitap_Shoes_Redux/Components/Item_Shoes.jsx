import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "../Redux/Constants";

class Item_Shoes extends Component {
  render() {
    let shoes = this.props.shoes;
    return (
      <div className="col-3 mt-5">
        <div className="card" style={{ height: "100%" }}>
          <span
            style={{
              position: "absolute",
              width: "60px",
              height: "25px",
              top: "0",
              right: "0",
              background: "black",
              borderRadius: "3px",
              color: "white",
            }}
          >
            &#36;{shoes.price}
          </span>
          <img className="card-img-top" src={shoes.image} alt="" />
          <div className="card-body d-flex flex-column justify-content-between">
            <h4 className="card-title">{shoes.name}</h4>
            <p className="card-text">
              {shoes.description.length < 50
                ? shoes.description
                : shoes.description.substring(0, 50) + "..."}
            </p>
            <button
              onClick={() => this.props.handleAddToCart(shoes)}
              className="btn btn-secondary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (item) => {
      dispatch({
        type: ADD_TO_CART,
        payload: item,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Shoes);
