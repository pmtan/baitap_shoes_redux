import React, { Component } from "react";
import { connect } from "react-redux";
import Cart from "./Cart";
import Item_Shoes from "./Item_Shoes";

class Baitap_Shoes extends Component {
  renderShoesList = () => {
    return this.props.shoesList.map((item, index) => {
      return <Item_Shoes shoes={item} key={index} />;
    });
  };
  render() {
    return (
      <div className="container py-5">
        {this.props.cart.length > 0 && <Cart />}
        <div className="row">{this.renderShoesList()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    shoesList: state.ShoesReducer.shoesList,
    cart: state.ShoesReducer.cart,
  };
};

export default connect(mapStateToProps)(Baitap_Shoes);
