import React, { Component } from "react";
import { connect } from "react-redux";
import { HANDLE_QUANTITY, XOA } from "../Redux/Constants";

class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} alt="" style={{ width: "80px" }} />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => this.props.handleQuantity(item.id, -1)}
              className="btn btn-secondary mx-3"
            >
              -
            </button>
            {item.quantity}
            <button
              onClick={() => this.props.handleQuantity(item.id, 1)}
              className="btn btn-secondary mx-3"
            >
              +
            </button>
          </td>
          <td>{item.price * item.quantity}</td>
          <td>
            <button
              onClick={() => this.props.handleRemove(item.id)}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Tên SP</th>
              <th>Hình ảnh</th>
              <th>Giá</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
          <tfoot>
            <tr>
              <td colSpan={5}></td>
              <td>
                <h5>Tổng Cộng:</h5>
              </td>
              <td>
                <h5>
                  {this.props.cart.reduce(
                    (total, item) => total + item.price * item.quantity,
                    0
                  )}
                </h5>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.ShoesReducer.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (itemID) => {
      dispatch({
        type: XOA,
        payload: itemID,
      });
    },
    handleQuantity: (itemID, number) => {
      dispatch({
        type: HANDLE_QUANTITY,
        payload: { id: itemID, number: number },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
