import logo from './logo.svg';
import './App.css';
import Baitap_Shoes from './Baitap_Shoes_Redux/Components/Baitap_Shoes';
import React, { StrictMode } from 'react';

function App() {
  return (
    <div className="App">
      <React.StrictMode>
        <Baitap_Shoes/>
      </React.StrictMode>
    </div>
  );
}

export default App;
